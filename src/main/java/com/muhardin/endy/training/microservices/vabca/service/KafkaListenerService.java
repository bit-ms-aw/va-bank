package com.muhardin.endy.training.microservices.vabca.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.training.microservices.vabca.dto.TagihanRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaListenerService {
    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(
            autoStartup = "${kafka.enabled:false}",
            topics = "${kafka.topic.tagihan.request:tagihan-request-dev}"
    )
    public void terimaTagihanRequest(String msg) throws JsonProcessingException {
        log.info("Terima message : {}", msg);
        TagihanRequest tr = objectMapper.readValue(msg, TagihanRequest.class);
        log.info("Tagihan request : {}", tr);
    }
}
