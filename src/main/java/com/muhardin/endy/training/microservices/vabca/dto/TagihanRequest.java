package com.muhardin.endy.training.microservices.vabca.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TagihanRequest {
    private String nomorPelanggan;
    private String nama;
    private String email;
    private String noHp;
    private String keteranganTagihan;
    private String nilaiTagihan;
    private LocalDate jatuhTempo;
}
